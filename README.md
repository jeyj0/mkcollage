# mkcollage

A simple CLI tool to generate a collage from a directory of images.

## Usage

```sh
mkcollage INPUT_DIR OUTPUT_FILE
```

The script will always output a png image, no matter what the output filename is set to.

## Installation

1. First, make sure you have installed nix (https://nixos.org/download.html).
2. Run `nix-build default.nix`
3. Copy the resulting binary found in `./result/bin/mkcollage` into a directory that is in your path, or run it directly.
