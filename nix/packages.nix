{ sources ? import ./sources.nix }:
let
  haskellDeps = import ../dependencies.nix;
  compiler = "ghc8104";

  overlay = self: super: {
    niv = (import sources.niv {}).niv;

    ghc = self.haskell.packages.${compiler}.ghcWithPackages haskellDeps;
    haskell-language-server = self.haskell.packages.${compiler}.haskell-language-server;
  };
in
  import sources.nixpkgs {
    overlays = [ overlay ];
    config = {};
  }
