module Util where

import Turtle

infixl 0 |>
(|>) :: a -> (a -> b) -> b
arg |> fun = fun arg

list :: Fold a [a]
list = Fold (\x a -> x . (a:)) id ($ [])
