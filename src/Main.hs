{-# LANGUAGE OverloadedStrings #-}
module Main where

import Turtle hiding ((%))
import Codec.Picture
import Codec.Picture.Extra
import Data.Text.IO (putStrLn)
import Prelude hiding (FilePath, putStrLn)
import Data.Either (rights) 
import Data.Ratio
import Util

desiredHeight :: Int
desiredHeight = 200

maximumWidth :: Int
maximumWidth = 2000

main :: IO ()
main = do
  args <- arguments

  case args of
    [] -> showHelp
    [_] -> showHelp
    _:_:_:_ -> showHelp
    [inputDir, outputFile] -> do
      let inputDir' = fromText inputDir
      let outputFile' = fromText outputFile
      makeCollage inputDir' outputFile'

showHelp :: IO ()
showHelp = putStrLn "Usage: mkcollage INPUT_DIR OUTPUT_FILE"

makeCollage :: FilePath -> FilePath -> IO ()
makeCollage inputFolder outputFile = do
  images <- getImagesFromInputFolder inputFolder
  let collage = arrange images
  savePngImage (encodeString outputFile) collage
  putStrLn "Done."
  where
    getImagesFromInputFolder :: FilePath -> IO [Image PixelRGB8]
    getImagesFromInputFolder inputFolder = do
      paths <- reduce list $ ls inputFolder
      let imagePaths = filter isImagePath paths
      eitherImages <- mapM (readImage . encodeString) imagePaths
      pure $ map convertRGB8 $ rights eitherImages
      where
        isImagePath :: FilePath -> Bool
        isImagePath path = case extension path of
          Nothing -> False
          Just ext -> case ext of
            "png" -> True
            "jpg" -> True
            _ -> False

    arrange :: [Image PixelRGB8] -> DynamicImage
    arrange images =
      let
        scaledImages = map scaleToSize images
      in
      ImageRGB8 $ beside $ reverse scaledImages
      where
        scaleToSize :: Image PixelRGB8 -> Image PixelRGB8
        scaleToSize original =
          let
            originalWidth = imageWidth original % 1
            originalHeight = imageHeight original % 1
            heightFactor = (desiredHeight % 1) / originalHeight
            scaledWidth = floor $ originalWidth * heightFactor
          in
          if scaledWidth > maximumWidth
            then scaleToWidth originalWidth originalHeight original
            else scaleBilinear scaledWidth desiredHeight original
          where
            scaleToWidth :: Ratio Int -> Ratio Int -> Image PixelRGB8 -> Image PixelRGB8
            scaleToWidth originalWidth originalHeight original =
              let
                widthFactor = (maximumWidth % 1) / originalWidth
                scaledHeight = floor $ originalHeight * widthFactor
              in
                scaleBilinear maximumWidth scaledHeight original
