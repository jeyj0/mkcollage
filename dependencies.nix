hpkgs: with hpkgs; [
  turtle
  JuicyPixels
  JuicyPixels-extra

  hspec
  QuickCheck
]
