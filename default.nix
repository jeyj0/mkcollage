let
  pkgs = import ./nix/packages.nix {};
in
  pkgs.stdenv.mkDerivation {
    name = "mkcollage";

    src = ./.;

    buildPhase = ''
      make build
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp out/main $out/bin/mkcollage
    '';

    buildInputs = with pkgs; [
      ghc
      gnumake
    ];
  }
